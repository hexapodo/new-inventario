import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { User } from '../entity/User';
import { hash, genSalt, compare } from 'bcrypt';
import { config } from '../config';
import { sign } from 'jsonwebtoken';
import { Permiso } from '../entity/Permiso';


export const login = async (req: Request, res: Response): Promise<Response> => {
    const username = req.body.username;
    const password = req.body.password;

    if (username === undefined || password === undefined) {
        const response: {[k: string]: any} =  {
            success: false,
            user: null
        };
        response[config.auth.tokenKey] = null;
        res.statusCode = 400;
        return res.json(response);
    }

    const user = await getRepository(User).findOne({ where: { login: username }, relations: ['roles', 'roles.permisos'] });
    if (user) {
        const success = await compare(password, user.password);
        if (success) {
            const token = sign({id: user.id}, config.auth.secret, {
                expiresIn: config.auth.time
            });
            const roles = user.roles;
            let permisos: Permiso[] = [];
            roles.forEach(role => {
                role.permisos.forEach(permiso => {
                    permisos.push(permiso);
                });
            });
            const response: {[k: string]: any} =  {
                success,
                user: {
                    name: user.name,
                    email: user.email,
                    phone: user.phone
                },
                permisos: permisos
            };
            response[config.auth.tokenKey] = token;
            return res.json(response);
        }
        const response: {[k: string]: any} =  {
            success,
            user: null
        };
        response[config.auth.tokenKey] = null;
        res.statusCode = 404;
        return res.json(response);
    } else {
        const response: {[k: string]: any} =  {
            success: false,
            user: null
        };
        response[config.auth.tokenKey] = null;
        res.statusCode = 404;
        return res.json(response);
    }
}

export const resetPass = async (req: Request, res: Response): Promise<Response> => {
    // TODO
    genSalt(config.encryp.saltRounds, (err, salt) => {
        console.log('salt', salt);
        hash('123456', salt, function(err, hash) {
            console.log('hash', hash);
        });
    });
    return res.json();
}
