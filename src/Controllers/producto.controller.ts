import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { Producto } from '../entity/Producto';

export class ProductoController {
    static getProductos = async (req: Request, res: Response): Promise<Response> => {
        const productos = await getRepository(Producto).find({ relations: ['fotos', 'presentaciones', 'presentaciones.fotos'] });
        return res.json(productos);
    }

    static getProducto = async (req: Request, res: Response): Promise<Response> => {
        const producto = await getRepository(Producto).findOne(req.params.id, { relations: ['fotos', 'presentaciones', 'presentaciones.fotos'] });
        if (producto) {
            return res.json(producto);
        }
        return res.status(404).json({ msg: 'Producto no encontrado' });
    }

    static createProducto = async (req: Request, res: Response): Promise<Response> => {
        const newProducto = getRepository(Producto).create(req.body);
        const results = await getRepository(Producto).save(newProducto);
        return res.json(results);
    }

    static modifyProducto = async (req: Request, res: Response): Promise<Response> => {
        const producto = await getRepository(Producto).findOne(req.params.id);
        if (producto) {
            console.log('modifyProducto');
            getRepository(Producto).merge(producto, req.body);
            const results = await getRepository(Producto).save(producto);
            return res.json(results);
        }
        return res.status(404).json({ msg: 'producto no encontrado' });
    }

    static deleteProducto = async (req: Request, res: Response): Promise<Response> => {
        const result = await getRepository(Producto).delete(req.params.id);
        return res.json(result);
    }
}


