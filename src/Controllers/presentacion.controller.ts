import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { Presentacion } from '../entity/Presentacion';
import { Producto } from '../entity/Producto';

export class PresentacionController {
    static getPresentaciones = async (req: Request, res: Response): Promise<Response> => {
        const presentaciones = await getRepository(Presentacion).find({ relations: ['fotos'] });
        return res.json(presentaciones);
    }

    static getPresentacionesByProducto = async (req: Request, res: Response): Promise<Response> => {
        const producto = await getRepository(Producto).findOne(req.params.id, { relations: ['presentaciones'] });
        console.log(producto?.nombre);
        if (!producto) {
            res.status(404);
        }
        return res.json(producto?.presentaciones);
    }

    static getPresentacion = async (req: Request, res: Response): Promise<Response> => {
        const presentacion = await getRepository(Presentacion).findOne(req.params.id, { relations: ['fotos'] });
        if (presentacion) {
            return res.json(presentacion);
        }
        return res.status(404).json({ msg: 'Presentacion no encontrada' });
    }

    static createPresentacion = async (req: Request, res: Response): Promise<Response> => {
        const newPresentacion = getRepository(Presentacion).create(req.body);
        const results = await getRepository(Presentacion).save(newPresentacion);
        return res.json(results);
    }

    static modifyPresentacion = async (req: Request, res: Response): Promise<Response> => {
        const presentacion = await getRepository(Presentacion).findOne(req.params.id);
        if (presentacion) {
            console.log('modifyPresentacion');
            getRepository(Presentacion).merge(presentacion, req.body);
            const results = await getRepository(Presentacion).save(presentacion);
            return res.json(results);
        }
        return res.status(404).json({ msg: 'presentacion no encontrada' });
    }

    static deletePresentacion = async (req: Request, res: Response): Promise<Response> => {
        const result = await getRepository(Presentacion).delete(req.params.id);
        return res.json(result);
    }
}


