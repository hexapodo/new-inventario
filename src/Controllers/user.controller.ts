import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { User } from '../entity/User';

export class UserController {
    static getUsers = async (req: Request, res: Response): Promise<Response> => {
        const users = await getRepository(User).find({ relations: ['roles'] });
        return res.json(users);
    }

    static getUser = async (req: Request, res: Response): Promise<Response> => {
        const user = await getRepository(User).findOne(req.params.id, { relations: ['roles'] });
        if (user) {
            return res.json(user);
        }
        return res.status(404).json({ msg: 'user not found' });
    }

    static createUser = async (req: Request, res: Response): Promise<Response> => {
        const newUser = getRepository(User).create(req.body);
        const results = await getRepository(User).save(newUser);
        return res.json(results);
    }

    static modifyUser = async (req: Request, res: Response): Promise<Response> => {
        const user = await getRepository(User).findOne(req.params.id);
        if (user) {
            console.log('modifyUser');
            getRepository(User).merge(user, req.body);
            const results = await getRepository(User).save(user);
            return res.json(results);
        }
        return res.status(404).json({ msg: 'user not found' });
    }

    static deleteUser = async (req: Request, res: Response): Promise<Response> => {
        const result = await getRepository(User).delete(req.params.id);
        return res.json(result);
    }
}


