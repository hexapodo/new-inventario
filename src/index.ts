import 'reflect-metadata';

import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import { createConnection } from 'typeorm';

import loginRouter from './routes/login.routes';
import productoRouter from './routes/producto.routes';
import presentacionRouter from './routes/presentacion.routes';
import userRouter from './routes/user.routes';
import roleRouter from './routes/role.routes';

const app = express();
createConnection();

// middleware
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

// routes
app.use(loginRouter);
app.use(productoRouter);
app.use(presentacionRouter);
app.use(userRouter);
app.use(roleRouter);

app.listen(3001);
console.log('Server ok');