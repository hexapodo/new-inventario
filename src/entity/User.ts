import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToOne } from 'typeorm';
import { Role } from './Role';
import { Repartidor } from './Repartidor';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    phone: string;

    @Column()
    login: string;

    @Column()
    password: string;

    @Column()
    salt: string;

    @ManyToMany(type => Role, role => role.users)
    @JoinTable()
    roles: Role[];

    @OneToOne(type => Repartidor, repartidor => repartidor.user)
    repartidor: Repartidor;
}