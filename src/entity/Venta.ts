import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { Repartidor } from './Repartidor';
import { Encargo } from './Encargo';
import { Detalle } from './Detalle';

@Entity()
export class Venta {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'datetime'})
    fecha: string;

    @ManyToOne( type => Repartidor, repartidor => repartidor.ventas)
    repartidor: Repartidor;

    @ManyToOne( type => Encargo, encargo => encargo.ventas)
    encargo: Encargo;

    @OneToMany( type => Detalle, detalle => detalle.venta)
    detalles: Detalle[];
}
