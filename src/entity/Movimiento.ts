import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne} from "typeorm";
import { User } from "./User";
import { Encargo } from "./Encargo";
import { Detalle } from "./Detalle";
import { Presentacion } from "./Presentacion";

@Entity()
export class Movimiento {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    cantidad: number;

    @Column()
    precio: number;

    @Column()
    existencia: number;

    @ManyToOne(type => Encargo, encargo => encargo.movimientos)
    encargo: Encargo;

    @OneToOne(type => Detalle, detalle => detalle.movimiento)
    detalle: Detalle;

    @ManyToOne(type => Presentacion, presentacion => presentacion.movimientos)
    presentacion: Presentacion;

}
