import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { Venta } from './Venta';
import { Movimiento } from './Movimiento';

@Entity()
export class Detalle {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @ManyToOne( type => Venta, venta => venta.detalles)
    venta: Venta;

    @OneToOne(type => Movimiento, movimiento => movimiento.detalle)
    @JoinColumn()
    movimiento: Movimiento;


}