import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { Foto } from './Foto';
import { Producto } from './Producto';
import { Movimiento } from './Movimiento';

@Entity()
export class Presentacion {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

    @Column()
    cantidad: number;

    @OneToMany( type => Foto, foto => foto.presentacion)
    fotos: Foto[];

    @ManyToOne( type => Producto, producto => producto.presentaciones)
    producto: Producto;

    @OneToMany( type => Movimiento, movimiento => movimiento.presentacion)
    movimientos: Movimiento[];

}