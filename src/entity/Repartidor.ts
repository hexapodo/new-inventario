import { Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { Encargo } from './Encargo';
import { Venta } from './Venta';
import { User } from './User';

@Entity()
export class Repartidor {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToMany( type => Encargo, encargo => encargo.repartidor)
    encargos: Encargo[];

    @OneToMany( type => Venta, venta => venta.repartidor)
    ventas: Venta[];

    @OneToOne(type => User, user => user.repartidor)
    @JoinColumn()
    user: User;
}