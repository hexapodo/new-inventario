import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Producto } from './Producto';
import { Presentacion } from './Presentacion';

@Entity()
export class Foto {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    url: string;

    @ManyToOne(type => Producto, producto => producto.fotos)
    producto: Producto;

    @ManyToOne(type => Presentacion, presentacion => presentacion.fotos)
    presentacion: Presentacion;
}