import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";
import { User } from "./User";
import { Permiso } from "./Permiso";

@Entity()
export class Role {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    role: string;

    @Column()
    code: string;

    @ManyToMany(type => User, user => user.roles)
    users: User[];

    @ManyToMany(type => Permiso, permiso => permiso.roles)
    @JoinTable()
    permisos: Permiso[];

}
