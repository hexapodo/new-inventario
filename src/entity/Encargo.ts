import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { Repartidor } from './Repartidor';
import { Venta } from './Venta';
import { Movimiento } from './Movimiento';

@Entity()
export class Encargo {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'datetime'})
    fecha: string;

    @Column()
    activo: boolean;

    @ManyToOne( type => Repartidor, repartidor => repartidor.encargos)
    repartidor: Repartidor;

    @OneToMany( type => Venta, venta => venta.encargo)
    ventas: Venta[];

    @OneToMany( type => Movimiento, movimiento => movimiento.encargo)
    movimientos: Movimiento[];
}