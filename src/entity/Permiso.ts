import {Entity, PrimaryGeneratedColumn, Column, ManyToMany} from "typeorm";
import { Role } from "./Role";

@Entity()
export class Permiso {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    permiso: string;

    @Column()
    code: string;

    @ManyToMany(type => Role, role => role.permisos)
    roles: Role[];

}
