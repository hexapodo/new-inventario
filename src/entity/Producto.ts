import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Foto } from './Foto';
import { Presentacion } from './Presentacion';

@Entity()
export class Producto {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    activo: boolean;

    @OneToMany( type => Foto, foto => foto.producto)
    fotos: Foto[];

    @OneToMany( type => Presentacion, presentacion => presentacion.producto)
    presentaciones: Presentacion[];

}