import { Router } from 'express';
import { ProductoController } from '../Controllers/producto.controller'
import { guard } from './guard';

const router = Router();

router.get('/productos', guard, ProductoController.getProductos);
router.get('/productos/:id', guard, ProductoController.getProducto);
router.post('/productos', guard, ProductoController.createProducto);
router.put('/productos/:id', guard, ProductoController.modifyProducto);
router.delete('/productos/:id', guard, ProductoController.deleteProducto);

export default router;