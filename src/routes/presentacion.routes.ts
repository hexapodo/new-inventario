import { Router } from 'express';
import { PresentacionController } from '../Controllers/presentacion.controller'
import { guard } from './guard';

const router = Router();

router.get('/presentaciones', guard, PresentacionController.getPresentaciones);
router.get('/presentaciones/byProducto/:id', guard, PresentacionController.getPresentacionesByProducto);
router.get('/presentaciones/:id', guard, PresentacionController.getPresentacion);
router.post('/presentaciones', guard, PresentacionController.createPresentacion);
router.put('/presentaciones/:id', guard, PresentacionController.modifyPresentacion);
router.delete('/presentaciones/:id', guard, PresentacionController.deletePresentacion);

export default router;