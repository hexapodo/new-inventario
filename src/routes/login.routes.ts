import { Router } from 'express';
import { login } from '../Controllers/login.controller'
import { guard } from './guard';

const router = Router();

router.post('/login', login);

export default router;