import { Router } from 'express';
const router = Router();

import { getRole, createRole, getRoles, modifyRole, deleteRole } from '../Controllers/role.controller'

router.get('/roles', getRoles);
router.get('/roles/:id', getRole);
router.post('/roles', createRole);
router.put('/roles/:id', modifyRole);
router.delete('/roles/:id', deleteRole);

export default router;